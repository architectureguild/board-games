﻿namespace BoardGames
{
    public enum ItemStatus
    {
        Hidden = 0,
        Revealed = 1,
    }
}
