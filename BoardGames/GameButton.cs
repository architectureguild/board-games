﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BoardGames
{
    public class GameButton : Button
    {
        public ItemType Type { get; set; }
        public ItemStatus Status { get; set; }
        public string Value { get; set; }
        public ICollection<GameButton> Neighbors { get; set; }

        public GameButton()
        {
            this.Click += new EventHandler(Reveal);
            this.Font = new Font(this.Font.FontFamily, 20);
            Status = ItemStatus.Hidden;
            Type = ItemType.None;
            Neighbors = new List<GameButton>();
        }

        public void ShowNeighbors(object sender, EventArgs e)
        {
            foreach (var neighbor in Neighbors)
            {
                neighbor.FlatStyle = FlatStyle.Popup;
            }
        }

        public void Reveal(object sender, EventArgs e)
        {
            Status = ItemStatus.Revealed;
            FlatStyle = FlatStyle.Flat;
            Text = Value;
            TextAlign = ContentAlignment.MiddleCenter;

            if (Type == ItemType.None)
            {
                foreach (var neighbor in Neighbors.Where(n => n.Status != ItemStatus.Revealed))
                {
                    neighbor.Reveal(sender, e);
                }
            }
        }

        public void AddNeighbor(GameButton b)
        {
            if (b != null)
                Neighbors.Add(b);
        }

        public void CreateMine()
        {
            Value = "☼";
            Type = ItemType.Mine;
        }

        public void CreateValue()
        {
            if (Type == ItemType.Mine)
                return;

            var value = Neighbors.Count(b => b.Type == ItemType.Mine);

            if (value == 0)
            {
                Value = String.Empty;
                Type = ItemType.None;
            }

            if (value > 0)
            {

                Value = value.ToString();
                Type = ItemType.Number;
            }
        }
    }
}
