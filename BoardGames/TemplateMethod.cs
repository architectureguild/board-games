﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BoardGames
{
    public abstract class TemplateMethod
    {
        public void Create()
        {
            SetupBoard();
            InstantiateObjects();
            ConfigureGameLogic();
            SetWinningConditions();
        }

        protected abstract void SetupBoard();

        protected abstract void SetWinningConditions();

        protected abstract void ConfigureGameLogic();

        protected abstract void InstantiateObjects();
    }
}
