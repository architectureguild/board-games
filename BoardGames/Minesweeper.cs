﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BoardGames
{
    public partial class Minesweeper : Form
    {
        private const int _size = 50;
        private const int Side = 15;
        private const int HPadding = 15;
        private const int VPadding = 34;
        private readonly GameButton[,] _cells;

        public Minesweeper()
        {
            InitializeComponent();
            this.Height = (_size * Side) + VPadding;
            this.Width = (_size * Side) + HPadding;
            _cells = new GameButton[Side, Side];
        }

        private void Minesweeper_Load(object sender, EventArgs e)
        {
            for (var i = 0; i < Side; i++)
            {
                for (var j = 0; j < Side; j++)
                {
                    var b = new GameButton
                    {
                        Parent = this,
                        Top = i * _size,
                        Left = j * _size,
                        Width = _size,
                        Height = _size
                    };
                    _cells[i, j] = b;
                }
            }

            CreateMines();
            CreateNeighbors();
            CountMines();

            for (var i = 0; i < Side; i++)
            {
                for (var j = 0; j < Side; j++)
                {
                    var cell = _cells[i, j];
                    //cell.Reveal(cell, null);
                }
            }
        }

        private void CountMines()
        {
            for (var i = 0; i < Side; i++)
            {
                for (var j = 0; j < Side; j++)
                {
                    var cell = _cells[i, j];
                    cell.CreateValue();
                }
            }
        }

        private void CreateMines()
        {
            var rnd = new Random(DateTime.Now.Millisecond);
            for (var i = 0; i < Side; i++)
            {
                for (var j = 0; j < Side; j++)
                {
                    var cell = _cells[i, j];
                    if (rnd.Next(100) > 80)
                    {
                        cell.CreateMine();
                    }
                }
            }
        }

        /// <summary>
        /// |n1|n2|n3|
        /// |n4|☼ |n5|
        /// |n6|n7|n8|
        /// </summary>
        /// <param name="cells"></param>
        private void CreateNeighbors()
        {
            var neighbors = new List<int>();

            for (var i = 0; i < Side; i++)
            {
                for (var j = 0; j < Side; j++)
                {
                    var cell = _cells[i, j];

                    for (var nx = -1; nx <= 1; nx++)
                    {
                        for (var ny = -1; ny <= 1; ny++)
                        {
                            try
                            {
                                cell.AddNeighbor(_cells[i + nx, j + ny]);
                            }
                            catch (Exception e)
                            {
                                continue;
                            }
                        }
                    }
                }
            }
        }
    }
}
